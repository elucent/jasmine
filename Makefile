.PHONY: debug tests

CXX=clang++
DEBUGFLAGS=-g3 -std=c++11 -Wall -Wno-char-subscripts
RELEASEFLAGS=-O3 -std=c++11 -Wall -Wno-char-subscripts
debug:
	${CXX} ${DEBUGFLAGS} -Iinclude src/**/*.cpp src/Jasmine.cpp -o jasmine

release:
	${CXX} ${RELEASEFLAGS} -Iinclude src/**/*.cpp src/Jasmine.cpp -o jasmine

tests:
	${CXX} ${DEBUGFLAGS} -Iinclude -Ilib/include lib/src/*.cpp src/**/*.cpp src/Tests.cpp -o tests
