#include "Jasmine.h"

namespace jasmine {
    using namespace std;

    x86Operand x86ParseOperand(TokenCache& cache) {
        if (cache.next().getType() == TokenType::PERCENT) {
            cache.pop();
            if (cache.next().getType() == TokenType::STRING) {
                const string& s = cache.next().getValue();
                const char* ptr = &s[0];
                x86Size size;
                x86Register reg;
                if (s.size() == 2) {
                    if (s[1] == 'x' || s[1] == 'p' || s[1] == 'i') size = x86Size::WORD;
                    else if (s[1] == 'l' || s[1] == 'h') size = x86Size::BYTE;
                }
                else if (s.size() == 3) {
                    if (s[0] == 'e') size = x86Size::DWORD;
                    else if (s[0] == 'r') size = x86Size::QWORD;
                    else throw ParseError(Format("Invalid register name ", s, "."), cache.next().getLine());
                    ptr ++;
                    if (s[2] != 'x' && s[2] != 'i' && s[2] != 'p') throw ParseError(Format("Invalid register name ", s, "."), cache.next().getLine());
                }
                else throw ParseError(Format("Invalid register name ", s, "."), cache.next().getLine());
                
                switch(ptr[0]) {
                    case 'a': reg = x86Register::A; break;
                    case 'c': reg = x86Register::C; break;
                    case 'b': reg = ptr[1] == 'p' ? x86Register::BP : x86Register::B; break;
                    case 'd': reg = ptr[1] == 'i' ? x86Register::DI : x86Register::D; break;
                    case 's': reg = ptr[1] == 'p' ? x86Register::SP : x86Register::SI; break;
                    default: throw ParseError(Format("Invalid register name ", s, "."), cache.next().getLine());
                }

                cache.pop();
                return { reg, size };
            }
            else throw ParseError(Format("Expected register name following '%', found '", cache.next().getValue(), "'."), cache.next().getLine());
        }
        else if (cache.next().getType() == TokenType::DOLLAR) {
            cache.pop();
            int32_t i = 1;
            if (cache.next().getType() == TokenType::MINUS) {
                cache.pop();
                i = -1;
            }
            if (cache.next().getType() == TokenType::INT) {
                i *= stoi(cache.pop().getValue());
                return { i };
            }
            else throw ParseError(Format("Expected immediate value following '$', found '", cache.next().getValue(), "'."), cache.next().getLine());
        }
        else if (cache.next().getType() == TokenType::INT) {
            int i = stoi(cache.pop().getValue());
            if (cache.next().getType() == TokenType::LPAREN) {
                cache.pop();
                x86Operand op = x86ParseOperand(cache);
                op.setOffset(i);
                if (cache.pop().getType() == TokenType::RPAREN) {
                    return op;
                }
            }
            throw ParseError(Format("Expected parentheses surrounding register for offset."), cache.next().getLine());
        }
        else if (cache.next().getType() == TokenType::MINUS) {
            cache.pop();
            if (cache.next().getType() != TokenType::INT) throw ParseError(Format("Expected integer offset following '-', found '", cache.next().getValue(), "'."), cache.next().getLine());
            x86Operand op = x86ParseOperand(cache);
            op.setOffset(-op.getOffset());
            return op;
        }
        else throw ParseError(Format("Unexpected token '", cache.next().getValue(), "'."), cache.next().getLine());
    }

    x86Instruction x86ParseInstruction(TokenCache& cache) {
        if (cache.next().getType() == TokenType::STRING) {
            x86Opcode op = x86Opcode::NONE;
            x86Size size = x86Size::NONE;

            const string& s = cache.next().getValue();

            auto it = X86_MNEMMAP.find(s);
            if (it != X86_MNEMMAP.end()) {
                op = it->second;
                size = x86Size::AUTO;
            }
            else if ((it = X86_MNEMMAP.find(s.substr(0, s.size()-1))) != X86_MNEMMAP.end()) {
                op = it->second;
                switch (s.back()) {
                    case 'b':
                        size = x86Size::BYTE;
                        break;
                    case 'w':
                        size = x86Size::WORD;
                        break;
                    case 'l':
                        size = x86Size::DWORD;
                        break;
                    case 'q':
                        size = x86Size::QWORD;
                        break;
                    default:
                        throw ParseError(Format("Unknown size suffix char '", s.back(), "' on instruction '", s, "'."), cache.next().getLine());
                }
            }

            if (op != x86Opcode::NONE) {
                x86Operand lhs, rhs;
                cache.pop();
                int line = cache.next().getLine();
                switch (op) {
                    case x86Opcode::ADD:
                    case x86Opcode::OR:
                    case x86Opcode::ADC:
                    case x86Opcode::SBB:
                    case x86Opcode::AND:
                    case x86Opcode::SUB:
                    case x86Opcode::XOR:
                    case x86Opcode::CMP:
                    case x86Opcode::MOV:
                    case x86Opcode::XCHG:
                    case x86Opcode::MUL:
                    case x86Opcode::IMUL:
                        lhs = x86ParseOperand(cache);
                        if (cache.pop().getType() != TokenType::COMMA)
                            throw ParseError(Format("Expected comma separating arguments in instruction '", s, "'."), cache.next().getLine());
                        rhs = x86ParseOperand(cache);
                        if (cache.pop().getType() != TokenType::NEWLINE)
                            throw ParseError(Format("Expected line terminator after complete instruction, found '", cache.next().getValue(), "'."), cache.next().getLine());
                        return { op, size, lhs, rhs, line };
                    case x86Opcode::PUSH:
                    case x86Opcode::POP:
                    case x86Opcode::INC:
                    case x86Opcode::DEC:
                    case x86Opcode::JMP:
                    case x86Opcode::LEA:
                    case x86Opcode::NOT:
                    case x86Opcode::NEG:
                    case x86Opcode::CALL:
                        lhs = x86ParseOperand(cache);
                        if (cache.pop().getType() != TokenType::NEWLINE)
                            throw ParseError(Format("Expected line terminator after complete instruction, found '", cache.next().getValue(), "'."), cache.next().getLine());
                        return { op, size, lhs, line };

                    case x86Opcode::NOP:
                    case x86Opcode::DIV:
                    case x86Opcode::IDIV:
                    case x86Opcode::RET:
                        if (cache.pop().getType() != TokenType::NEWLINE)
                            throw ParseError(Format("Expected line terminator after complete instruction, found '", cache.next().getValue(), "'."), cache.next().getLine());
                        return { op, size, line };

                    default:
                        break;
                }
            }
            throw ParseError(Format("Unknown instruction '", s, "'."), cache.next().getLine());
        }
        else throw ParseError(Format("Expected mnemonic at start of instruction, found '", cache.next().getValue(), "'."), cache.next().getLine());
    }

    vector<x86Instruction> x86Parse(TokenCache& cache) {
        vector<x86Instruction> instructions;
        while (cache) {
            instructions.push_back(x86ParseInstruction(cache));
        }
        return instructions;
    }

    ParseError::ParseError(const string& message_in, int line) noexcept 
        : BasicError(message_in, line, "SYNTAX") {
        //
    }
}