#include "Jasmine.h"

namespace jasmine {
    using namespace std;

    unordered_map<string, x86Opcode> x86GenerateMnemonicMap() {
        unordered_map<string, x86Opcode> toReturn;
        for (int i = 0; i < sizeof(X86_MNEMONICS) / sizeof(const char*); i ++) 
            toReturn.insert({ X86_MNEMONICS[i], (x86Opcode)i });
        return toReturn;
    }

    // x86Operand

    x86Size x86Operand::GetImmediateSize(int32_t immediate) {
        if (immediate < 128 && immediate > -129) return x86Size::BYTE;
        else if (immediate < 32768 && immediate > -32769) return x86Size::WORD;
        else return x86Size::DWORD;
    }

    x86Operand::x86Operand() 
        : reg(x86Register::NONE), size(x86Size::NONE), offset(0) {
        //
    }

    x86Operand::x86Operand(x86Register reg_in, x86Size size_in) 
        : reg(reg_in), size(size_in), offset(0) {
        //
    }
    
    x86Operand::x86Operand(int32_t immediate_in)
        : reg(x86Register::IMM), size(GetImmediateSize(immediate_in)), offset(immediate_in) {
        //
    }  
    
    bool x86Operand::isImmediate() const {
        return reg == x86Register::IMM;
    }
    
    int32_t x86Operand::getOffset() const {
        return offset;
    }

    void x86Operand::setOffset(int32_t immediate) {
        offset = immediate;
    }
    
    x86Size x86Operand::getSize() const {
        return size;
    }
    
    x86Register x86Operand::getRegister() const {
        return reg;
    }

    string x86GetRegName(x86Register reg, x86Size size) {
        string s = "";
        if (size == x86Size::QWORD) s = "r";
        if (size == x86Size::DWORD) s = "e";

        static const char* names[] = { "ax", "cx", "dx", "bx", "sp", "bp", "si", "di" };
        s += names[(int)reg];
        if (size == x86Size::BYTE) s.back() = 'l';
        return s;
    }

    ostream& operator<<(ostream& os, x86Operand operand) {
        if (operand.isImmediate())
            return os << "$" << operand.getOffset();
        else if (operand.getOffset() != 0)
            return os << operand.getOffset() << "(%" << x86GetRegName(operand.getRegister(), operand.getSize()) << ")";
        else return os << "%" << x86GetRegName(operand.getRegister(), operand.getSize());
    }

    // x86Instruction
    
    x86Instruction::x86Instruction(x86Opcode op_in, x86Size size_in, int line_in) 
        : op(op_in), size(size_in), line(line_in) {
        //
    }
    
    x86Instruction::x86Instruction(x86Opcode op_in, x86Size size_in, x86Operand first_in, int line_in) 
        : op(op_in), size(size_in), first(first_in), line(line_in) {
        //
    }

    x86Instruction::x86Instruction(x86Opcode op_in, x86Size size_in, x86Operand first_in, x86Operand second_in, int line_in) 
        : op(op_in), size(size_in), first(first_in), second(second_in), line(line_in) {
        //
    }

    x86Opcode x86Instruction::getOpcode() const {
        return op;
    }

    x86Size x86Instruction::getSize() const {
        return size;
    }

    x86Operand x86Instruction::getFirst() const {
        return first;
    }

    x86Operand x86Instruction::getSecond() const {
        return second;
    }

    int x86Instruction::getLine() const {
        return line;
    }

    ostream& operator<<(ostream& os, x86Instruction insn) {
        os << "Line " << insn.getLine() << ": ";
        os << X86_MNEMONICS[(int)insn.getOpcode()];
        if (insn.getSize() != x86Size::AUTO) {
            switch (insn.getSize()) {
                case x86Size::BYTE:
                    os << 'b';
                    break;
                case x86Size::WORD:
                    os << 'w';
                    break;
                case x86Size::DWORD:
                    os << 'l';
                    break;
                case x86Size::QWORD:
                    os << 'q';
                    break;
                default:
                    break;
            }
        }
        os << " ";
        if (insn.getFirst().getRegister() != x86Register::NONE) os << insn.getFirst();
        if (insn.getSecond().getRegister() != x86Register::NONE) os << ", " << insn.getSecond();
        return os;
    }
}