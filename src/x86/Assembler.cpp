#include "Jasmine.h"

namespace jasmine {
    using namespace std;
    using namespace buf;

    unsigned char x86CreateModByte(unsigned char a, unsigned char b, unsigned char mode) {
        return (a & 7) | ((b & 7) << 3) | ((mode & 3) << 6);
    }

    void x86AddSizePrefix(Buffer& b, x86Size size) {
        switch (size) {
            case x86Size::WORD:
                b.push((unsigned char)0x66);
                break;
            case x86Size::QWORD:
                b.push((unsigned char)0x48);
                break;
            default:
                break;
        }
    }

    template<unsigned char regbase, unsigned char immbase>
    void x86SimpleBinaryAssembler(Buffer& b, const x86Instruction& insn) {
        x86Size size = insn.getSize();
        if (size == x86Size::AUTO) {
            if (insn.getSecond().getSize() == x86Size::NONE || insn.getSecond().getSize() == x86Size::AUTO)
                throw AssemblyError(Format("Cannot infer operation size from second operand '", insn.getSecond(), "'."), insn.getLine());
            size = insn.getSecond().getSize();
        }
        x86AddSizePrefix(b, size);

        if (insn.getSecond().isImmediate())
            throw AssemblyError(Format("Cannot perform operation on immediate value '$", insn.getSecond().getOffset(), "'!"), insn.getLine());

        if (insn.getFirst().isImmediate()) {
            unsigned char basecode = 0x80;
            if ((int)insn.getFirst().getSize() > (int)insn.getSecond().getSize())
                throw AssemblyError(Format("Size mismatch: cannot store immediate '$", insn.getFirst().getOffset(), "' in operand '", insn.getSecond(), "'."), insn.getLine());
            
            if (insn.getFirst().getSize() != x86Size::BYTE) basecode = 0x81;
            else if (insn.getSecond().getSize() != x86Size::BYTE) basecode = 0x83;

            int offset = insn.getSecond().getOffset();
            unsigned char mode = offset == 0 ? X86_REGISTER : X86_ONE_BYTE_DISPLACEMENT;
            if (offset != 0 && x86Operand::GetImmediateSize(insn.getSecond().getOffset()) != x86Size::BYTE) mode = X86_FOUR_BYTE_DISPLACEMENT;

            b.push(basecode);
            b.push(x86CreateModByte((unsigned char)insn.getSecond().getRegister(), immbase, mode));
            switch (insn.getFirst().getSize()) {
                case x86Size::BYTE:
                    b.push<char>(insn.getFirst().getOffset());
                    break;
                case x86Size::WORD:
                    b.push<short int>(insn.getFirst().getOffset());
                    break;
                case x86Size::DWORD:
                    b.push<int>(insn.getFirst().getOffset());
                    break;
                default:
                    break;
            }
            if (mode == X86_ONE_BYTE_DISPLACEMENT) b.push<char>(offset);
            else if (mode == X86_FOUR_BYTE_DISPLACEMENT) b.push<int>(offset);
        }
        else {
            unsigned char basecode = regbase;
            if ((int)insn.getFirst().getSize() > (int)insn.getSecond().getSize())
                throw AssemblyError(Format("Size mismatch: cannot store value '", insn.getFirst().getOffset(), "' in operand '", insn.getSecond(), "'."), insn.getLine());
            
            if (insn.getFirst().getOffset() != 0) basecode += 2;
            if (insn.getFirst().getSize() != x86Size::BYTE) basecode ++;

            int offset = 0;
            if (insn.getFirst().getOffset() != 0) offset = insn.getFirst().getOffset();
            if (insn.getSecond().getOffset() != 0) {
                if (offset != 0)
                    throw AssemblyError(Format("Cannot have two memory operands in one instruction."), insn.getLine());
                offset = insn.getSecond().getOffset();
            }

            x86Size offsetSize = x86Size::NONE;
            if (offset != 0) offsetSize = x86Operand::GetImmediateSize(offset);
            unsigned char mode = offset == 0 ? X86_REGISTER : X86_ONE_BYTE_DISPLACEMENT;
            if (offset != 0 && offsetSize != x86Size::BYTE) mode = X86_FOUR_BYTE_DISPLACEMENT;

            b.push(basecode);
            b.push(x86CreateModByte((unsigned char)insn.getSecond().getRegister(), (unsigned char)insn.getFirst().getRegister(), mode));
            if (mode == X86_ONE_BYTE_DISPLACEMENT) b.push<char>(offset);
            else if (mode == X86_FOUR_BYTE_DISPLACEMENT) b.push<int>(offset);
        }
    }

    void x86AssembleInstruction(Buffer& b, const x86Instruction& insn) {
        switch (insn.getOpcode()) {
            case x86Opcode::ADD:
                x86SimpleBinaryAssembler<0x00, 0>(b, insn);
                break;
            case x86Opcode::OR:
                x86SimpleBinaryAssembler<0x08, 1>(b, insn);
                break;
            case x86Opcode::ADC:
                x86SimpleBinaryAssembler<0x10, 2>(b, insn);
                break;
            case x86Opcode::SBB:
                x86SimpleBinaryAssembler<0x18, 3>(b, insn);
                break;
            case x86Opcode::AND:
                x86SimpleBinaryAssembler<0x20, 4>(b, insn);
                break;
            case x86Opcode::SUB:
                x86SimpleBinaryAssembler<0x28, 5>(b, insn);
                break;
            case x86Opcode::XOR:
                x86SimpleBinaryAssembler<0x30, 6>(b, insn);
                break;
            case x86Opcode::CMP:
                x86SimpleBinaryAssembler<0x38, 7>(b, insn);
                break;
        }
    }

    Buffer x86Assemble(const vector<x86Instruction>& instructions) {
        Buffer b;
        for (const x86Instruction& insn : instructions) {
            x86AssembleInstruction(b, insn);
        }
        return b;
    }

    AssemblyError::AssemblyError(const string& message_in, int line) noexcept 
        : BasicError(message_in, line, "CODEGEN") {
        //
    }
}