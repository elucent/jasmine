#include "Jasmine.h"
#include <fstream>
#include <iomanip>

using namespace std;
using namespace jasmine;

int main(int argc, char** argv) {
    if (argc != 2) return 1;

    ifstream is(argv[1]);
    TokenCache tokens = Lex(is);

    cout << "Tokens: " << endl;
    while (tokens) {
        cout << tokens.pop().getValue() << " ";
    }
    tokens.reset();
    cout << endl;

    auto insns = x86Parse(tokens);

    cout << "Parsed: " << endl;
    for (const x86Instruction &in : insns) cout << in << endl;

    cout << "Machine Code: " << endl;
    Buffer b = x86Assemble(insns);
    cout << hex << flush;
    while (b)
        cout << setw(2) << setfill('0') << (unsigned short int)b.read<unsigned char>() << " ";
    b.reset();
    cout << dec << endl;

    return 0;
}