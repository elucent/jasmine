#include "Jasmine.h"

namespace jasmine {
    using namespace std;

    // Token

    Token::Token(TokenType type_in, const string& value_in, int line_in) noexcept 
        : type(type_in), value(value_in), line(line_in) {
        //
    }

    TokenType Token::getType() const noexcept {
        return type;
    }

    const string& Token::getValue() const noexcept {
        return value;
    }

    int Token::getLine() const noexcept {
        return line;
    }

    // TokenCache

    TokenCache::TokenCache() 
        : index(0) {
        //
    }

    bool TokenCache::empty() const {
        return index == tokens.size();
    }

    TokenCache::operator bool() const {
        return !empty();
    }

    void TokenCache::append(const Token& token) {
        tokens.push_back(token);
    }

    const Token& TokenCache::next() {
        return tokens[index];
    }

    const Token& TokenCache::pop() {
        return tokens[index++];
    }

    void TokenCache::reset() {
        index = 0;
    }

    // LexicalError

    LexicalError::LexicalError(const string& message_in, int line) noexcept 
        : BasicError(message_in, line, "LEXICAL") {
        //
    }

    // Lexing

    TokenCache Lex(const string& s) {
        istringstream is(s);
        return Lex(is);
    }

    TokenCache Lex(istream& is) {
        static TokenType* mappings = nullptr;
        if (!mappings) {
            mappings = new TokenType[256];
            mappings[','] = TokenType::COMMA;
            mappings['$'] = TokenType::DOLLAR;
            mappings['#'] = TokenType::HASH;
            mappings[':'] = TokenType::COLON;
            mappings['%'] = TokenType::PERCENT;
            mappings['-'] = TokenType::MINUS;
            mappings['('] = TokenType::LPAREN;
            mappings[')'] = TokenType::RPAREN;
        }  

        TokenCache tokens;

        string buf;
        TokenType type = TokenType::NONE;
        int line = 0;
        bool dot = false;
        while (true) {
            if (!is.rdbuf()->in_avail()) break;

            char c = is.get();
            if (isalpha(c)) {
                type = TokenType::STRING;
                buf += c;
            }
            else if (isspace(c)) {
                if (type != TokenType::NONE) {
                    tokens.append({ type, buf, line });
                    buf.clear();
                    type = TokenType::NONE;
                }
                if (c == '\n') {
                    tokens.append({ TokenType::NEWLINE, "\n", line });
                    line ++;
                }
            }
            else if (isdigit(c)) {
                if (type == TokenType::NONE) {
                    type = TokenType::INT;
                    dot = false;
                }
                buf += c;
            }
            else if (c == '_') {
                if (type == TokenType::STRING) buf += c;
                else throw LexicalError(Format("Underscores are only allowed at the start of identifiers!"), line);
            }
            else if (c == '.') {
                if (type == TokenType::INT) {
                    if (dot) throw LexicalError(Format("Multiple dots in numeric literal '", buf, ".'!"), line);
                    else dot = true;
                    buf += c;
                }
                else {
                    if (type != TokenType::NONE) {
                        tokens.append({ type, buf, line });
                        buf.clear();
                        type = TokenType::NONE;
                    }
                    tokens.append({ TokenType::DOT, ".", line });
                }
            }
            else if (mappings[c] != TokenType::NONE) {
                if (type != TokenType::NONE) {
                    tokens.append({ type, buf, line });
                    buf.clear();
                    type = TokenType::NONE;
                }
                tokens.append({ mappings[c], string() + c, line });
            }
            else {
                throw LexicalError(Format("Unexpected symbol '", c, "'!"), line);
            }
        }

        return tokens;
    }
}