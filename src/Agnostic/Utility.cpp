#include "Jasmine.h"

namespace jasmine {
    using namespace std;

    // BasicError

    BasicError::BasicError(const string& message_in, int line, const string& label_in) noexcept 
        : message("[" + label_in + "] Line " + to_string(line) + ": " + message_in) {
        //
    }

    const char* BasicError::what() const noexcept {
        return message.c_str();
    }

    // Format
    
    ostringstream Format_os_;
}