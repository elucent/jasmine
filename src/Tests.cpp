#include "Testing.h"
#include "Agnostic/Lexer.h"
#include "Agnostic/Utility.h"
#include "x86/Assembler.h"
#include "x86/Parser.h"
#include <sstream>
#include <string>
using namespace test;
using namespace std;
using namespace jasmine;

/*
 * x86 Assembler
 */

Buffer x86FromString(const string& s) {
    TokenCache c = Lex(s);
    return x86Assemble(x86Parse(c));
}

void AssertBufferBytes(Buffer& b, const string& name, int c) {
    ASSERT_INEQUAL(b.length(), 0);
    ASSERT_EQUAL((int)b.read<unsigned char>(), c);
}

template<typename ...Args>
void AssertBufferBytes(Buffer& b, const string& name, int c, Args... args) {
    AssertBufferBytes(b, name, c);
    AssertBufferBytes(b, name, args...);
}

template<typename ...Args>
void AssertBufferBytes(const Buffer& buf, const string& name, int c, Args... args) {
    Buffer b = buf;
    AssertBufferBytes(b, name, c, args...);
}

TEST(immediate_binary_ops) {
    //
}

TEST(memory_binary_ops) {
    //
}

TEST(register_binary_ops) {
    AssertBufferBytes(x86FromString("add %eax, %ebx\n"), name, 0x01, 0xC3);
    AssertBufferBytes(x86FromString("xorq %rdx, %rsp\n"), name, 0x48, 0x31, 0xD4);
}

RUN();