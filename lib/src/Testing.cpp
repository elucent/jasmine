#include "Testing.h"

#include <iostream>

namespace test {
    std::map<std::string, __TESTFN> __TEST_FUNCS;
    std::map<std::string, bool> __TEST_RESULTS;

    bool __TEST_ALL() {
        for (auto &p : __TEST_FUNCS) __TEST_RESULTS[p.first] = true;

        std::cout << "Prepared to execute " << __TEST_FUNCS.size() << " tests:" << std::endl;
        std::cout << std::endl;
        for (auto &p : __TEST_FUNCS) {
            std::cout << "Running test '" << p.first << "'..." << std::endl;
            p.second(p.first);
            if (__TEST_RESULTS[p.first]) {
                std::cout << "    pass!" << std::endl;
            }
            else {
                std::cout << "    fail!" << std::endl;
            }
        }
        std::cout << std::endl;

        std::cout << "Final test results:" << std::endl;
        int count = 0;
        for (auto &p : __TEST_RESULTS) {
            std::cout << "Test '" << p.first << "': " << (p.second ? "pass!" : "fail!") << std::endl;
            if (p.second) count ++;
        }
        std::cout << count << " / " << __TEST_RESULTS.size() << " passed!" << std::endl;
        std::cout << std::endl;
        
        return true;
    }
}
