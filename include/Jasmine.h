#ifndef JASMINE_H
#define JASMINE_H

#include "Definitions.h"

#include "Agnostic/Lexer.h"
#include "Agnostic/Utility.h"

#include "x86/Assembler.h"
#include "x86/Instructions.h"
#include "x86/Parser.h"

namespace jasmine {
    using namespace std;

    //
}

#endif