#ifndef JASMINE_X86_INSTRUCTION_H
#define JASMINE_X86_INSTRUCTION_H

#include <iostream>
#include <unordered_map>

namespace jasmine {
    using namespace std;

    enum class x86Opcode {
        ADD,
        ADC,
        AND,
        CALL,
        CMP,
        DEC,
        DIV,
        IDIV,
        IMUL,
        INC,
        JMP,
        LEA,
        MOV,
        MUL,
        NEG,
        NOP,
        NOT,
        OR,
        POP,
        PUSH,
        RET,
        SBB,
        SUB,
        XCHG,
        XOR,
        NONE
    };

    static const char* X86_MNEMONICS[] = {
        "add",
        "adc",
        "and",
        "call",
        "cmp",
        "dec",
        "div",
        "idiv",
        "imul",
        "inc",
        "jmp",
        "lea",
        "mov",
        "mul",
        "neg",
        "nop",
        "not",
        "or",
        "pop",
        "push",
        "ret",
        "sbb",
        "sub",
        "xchg",
        "xor"
    };

    unordered_map<string, x86Opcode> x86GenerateMnemonicMap();

    static unordered_map<string, x86Opcode> X86_MNEMMAP = x86GenerateMnemonicMap();

    enum class x86Register {
        A, C, D, B, SP, BP, SI, DI, IMM, NONE
    };

    enum class x86Size {
        BYTE, WORD, DWORD, QWORD, AUTO, NONE
    };

    class x86Operand {
        x86Register reg;
        x86Size size;
        int32_t offset;
    public:
        static x86Size GetImmediateSize(int32_t immediate); 

        x86Operand();
        x86Operand(x86Register reg_in, x86Size size_in);
        x86Operand(x86Register reg_in, int32_t offset);
        x86Operand(int32_t immediate_in);

        bool isImmediate() const;
        int32_t getOffset() const;
        void setOffset(int32_t immediate);
        x86Size getSize() const;
        x86Register getRegister() const;
    };

    ostream& operator<<(ostream& os, x86Operand operand);

    class x86Instruction {
        x86Opcode op;
        x86Size size;
        x86Operand first, second;
        int line;
    public:
        x86Instruction(x86Opcode op_in, x86Size size_in, int line_in);
        x86Instruction(x86Opcode op_in, x86Size size_in, x86Operand first_in, int line_in);
        x86Instruction(x86Opcode op_in, x86Size size_in, x86Operand first_in, x86Operand second_in, int line_in);

        x86Opcode getOpcode() const;
        x86Size getSize() const;
        x86Operand getFirst() const;
        x86Operand getSecond() const;
        int getLine() const;
    };

    ostream& operator<<(ostream& os, x86Instruction insn);
}

#endif