#ifndef JASMINE_X86_ASSEMBLER_H
#define JASMINE_X86_ASSEMBLER_H

#include "Definitions.h"
#include "Buffer.h"

namespace jasmine {
    using namespace std;
    using namespace buf;

    static const unsigned char X86_REGISTER_INDIRECT = 0,
                               X86_ONE_BYTE_DISPLACEMENT = 1,
                               X86_FOUR_BYTE_DISPLACEMENT = 2,
                               X86_REGISTER = 3;

    Buffer x86Assemble(const vector<x86Instruction>& instructions);

    class AssemblyError : public BasicError {
    public:
        AssemblyError(const string& message_in, int line) noexcept;
    };
}

#endif