#ifndef JASMINE_X86_PARSER_H
#define JASMINE_X86_PARSER_H

#include "Definitions.h"
#include "Agnostic/Lexer.h"
#include "x86/Instructions.h"
#include <unordered_set>
#include <unordered_map>

namespace jasmine {
    using namespace std;

    x86Opcode x86GetOp(const string& mnemonic);
    vector<x86Instruction> x86Parse(TokenCache& tokens);

    class ParseError : public BasicError {
    public:
        ParseError(const string& message_in, int line) noexcept;
    };
}

#endif