#ifndef JASMINE_UTILITY_H
#define JASMINE_UTILITY_H

#include "Definitions.h"
#include <string>
#include <exception>
#include <iostream>
#include <sstream>
#include <cstring>
#include <cstdlib>

namespace jasmine {
    using namespace std;

    class BasicError : public exception {
        string message;
    public:
        BasicError(const string& message_in, int line, const string& label_in = "ERROR") noexcept;
    
        const char* what() const noexcept;
    };

    extern ostringstream Format_os_;

    class Format {
        template<typename T>
        void append(const T& t) {
            Format_os_ << t;
        }

        template<typename T, typename ...Args>
        void append(const T& t, Args... args) {
            Format_os_ << t;
            append(args...);
        }

    public:
        Format() {
            Format_os_.seekp(ios::beg);
        }

        template<typename T>
        Format(const T& t) : Format() {
            append(t);
        }

        template<typename ...Args>
        Format(Args... args) : Format() {
            append(args...);
        }

        operator string() const noexcept {
            return Format_os_.str().c_str();
        }

        operator const char*() const noexcept {
            return Format_os_.str().c_str();
        }
    };
}

#endif