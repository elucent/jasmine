#ifndef JASMINE_RUNTIME_H
#define JASMINE_RUNTIME_H

#include "Definitions.h"
#include "Agnostic/Utility.h"

namespace jasmine {
    using namespace std;

    class Block {
        void (*code)();

        static void (*allocate(const Buffer& buf))();
    public:
        Block();
        Block(const Buffer& buf);
        Block(const Block& other);
        Block& operator=(const Block& other);
        ~Block();

        void run();    
    };
}

#endif