#ifndef JASMINE_LEXER_H
#define JASMINE_LEXER_H

#include "Definitions.h"
#include "Agnostic/Utility.h"
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <exception>

namespace jasmine {
    using namespace std;

    enum class TokenType {
        NONE, NEWLINE, STRING, INT, COMMA, HASH, DOLLAR, COLON, DOT, PERCENT, MINUS, LPAREN, RPAREN
    };

    class Token {
        TokenType type;
        string value;
        int line;
    public:
        Token(TokenType type_in, const string& value_in, int line_in) noexcept;

        TokenType getType() const noexcept;
        const string& getValue() const noexcept;
        int getLine() const noexcept;
    };

    class TokenCache {
        vector<Token> tokens;
        unsigned long int index;
    public:
        TokenCache();
        bool empty() const;
        operator bool() const;
        void append(const Token& token);
        const Token& next();
        const Token& pop();
        void reset();

        friend ostream& operator<<(ostream& os, TokenCache& cache) {
            os << "{ ";
            for (Token& t : cache.tokens) os << t.getValue() << " ";
            return os << "}";
        }
    };

    class LexicalError : public BasicError {
    public:
        LexicalError(const string& message_in, int line) noexcept;
    };

    TokenCache Lex(const string& s);
    TokenCache Lex(istream& is);
}

#endif