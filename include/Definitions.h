#ifndef JASMINE_DEFINITIONS_H
#define JASMINE_DEFINITIONS_H

namespace jasmine {

    /**
     * 
     * Agnostic
     * 
     */

    // Lexer
    class Token;
    class LexicalError;

    // Runtime
    class Block;

    // Utility
    class BasicError;
    class Format;

    /**
     * 
     * x86 Target
     * 
     */

    // Assembler.h
    class AssemblyError;

    // Instructions.h
    enum class x86Register;
    enum class x86Size;
    class x86Operand;
    class x86Instruction;

    // Parser.h
    class ParseError;
}

#endif